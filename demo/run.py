import json
from time import sleep
from tango import DeviceProxy

plugin = "math.addnumbers"
kwargs = {"a": 1, "b": 2}

proxy = DeviceProxy("tango://localhost:10000/id00/dahu/1")
pid = proxy.startJob([plugin, json.dumps(kwargs)])
while proxy.getJobState(pid) == "starting":
    sleep(0.01)
proxy.waitJob(pid)
print("Output: %s" % proxy.getJobOutput(pid))
