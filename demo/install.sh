#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${SCRIPT_ROOT}/utils.sh

function main {
    source $(conda_root)/bin/activate $(conda_env)

    # install what we can with conda instead of pip
    conda install --file $SCRIPT_ROOT/requirements-conda.txt

    # install dahu
    local dahu_source=$HOME/dev/dahu
    find $dahu_source| grep -E "(__pycache__|\.pyc|\.pyo|\.egg-info$)" | xargs rm -rf
    python -m pip install $dahu_source  # NO DEV INSTALLATION!

    # install the demo project
    python -m pip install -e $SCRIPT_ROOT/..
}

main
