#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${SCRIPT_ROOT}/utils.sh


function create_conda_env {
    $(conda_root)/bin/conda config --env --add channels conda-forge
    $(conda_root)/bin/conda config --env --add channels tango-controls
    $(conda_root)/bin/conda create --prefix $(conda_env)
}


function main {
    create_conda_env
}


main
