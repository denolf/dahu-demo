#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${SCRIPT_ROOT}/utils.sh

function main {
    source $(conda_root)/bin/activate $(conda_env)

    export TANGO_HOST=localhost:10000
    export DAHU_PLUGINS=$(python -c "from dahu_demo import plugins;print(plugins.__path__[0])")
    dahu-register --instance dahu --member 1
    dahu-server dahu -v3
}

main