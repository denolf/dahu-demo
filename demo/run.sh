#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${SCRIPT_ROOT}/utils.sh

function main {
    source $(conda_root)/bin/activate $(conda_env)

    python ${SCRIPT_ROOT}/run.py
}

main