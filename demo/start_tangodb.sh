#!/bin/bash

SCRIPT_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


function print_endpoints {
    sleep 10
    echo ""
    echo "Tango database: http://localhost:10000"
}


function main {
    print_endpoints &

    sudo docker-compose -f ${SCRIPT_ROOT}/tango/docker-compose.yml up
}

main
