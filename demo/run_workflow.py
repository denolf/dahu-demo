import json
from time import sleep
from tango import DeviceProxy
from ewokscore.tests.examples.graphs import get_graph


plugin = "workflow.executegraph"
workflow, _ = get_graph("acyclic1")
varinfo = {"root_uri": "results"}
kwargs = {"graph": workflow, "binding": None, "varinfo": varinfo}

proxy = DeviceProxy("tango://localhost:10000/id00/dahu/1")
pid = proxy.startJob([plugin, json.dumps(kwargs)])
while proxy.getJobState(pid) == "starting":
    sleep(0.01)
proxy.waitJob(pid)
print("Output: %s" % proxy.getJobOutput(pid))
