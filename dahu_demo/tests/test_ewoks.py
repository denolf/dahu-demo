import pytest
from ewokscore.tests.examples.graphs import graph_names
from ewokscore.tests.examples.graphs import get_graph
from ewokscore.tests.utils.results import assert_execute_graph_all_tasks
from ewokscore import load_graph


@pytest.mark.parametrize("graph_name", graph_names())
def test_examples(graph_name, tmpdir, register_plugins):
    from dahu import job  # should be done after register_plugins

    g, expected = get_graph(graph_name)
    ewoksgraph = load_graph(g)

    if ewoksgraph.is_cyclic or ewoksgraph.has_conditional_links:
        binding = "ppf"
    else:
        binding = None
    varinfo = {"root_uri": str(tmpdir)}
    kwargs = {"graph": ewoksgraph.dump(), "binding": binding, "varinfo": varinfo}

    j = job.Job("workflow.executegraph", kwargs)
    j.start()
    j.join(timeout=10)
    print("Results:", j.output_data)

    if not ewoksgraph.is_cyclic:
        assert_execute_graph_all_tasks(ewoksgraph, expected, varinfo=varinfo)
