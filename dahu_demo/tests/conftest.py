import os
import pytest
from .. import plugins


@pytest.fixture(scope="session")
def register_plugins():
    os.environ["DAHU_PLUGINS"] = plugins.__path__[0]
