from dahu.plugin import Plugin
from dahu.factory import register


@register
class AddNumbers(Plugin):
    def process(self):
        self.output["sum"] = self.input["a"] + self.input["b"]
