from dahu.plugin import Plugin
from dahu.factory import register
from ewoks import execute_graph


@register
class ExecuteGraph(Plugin):
    def process(self):
        kwargs = dict(self.input)
        kwargs.pop("plugin_name")
        kwargs.pop("job_id")
        # job_id = kwargs.pop("job_id")
        # kwargs["execinfo"] = {"job_id": job_id}
        execute_graph(**kwargs)
        # self.output["result"] = ...
