# dahu-demo

Demo of job triggering with [Dahu](https://github.com/kif/dahu) ([docs](http://www.silx.org/doc/dahu/dev/))

## Demo scripts

Requires `conda` and docker.

* create.sh: create an environment (conda)
* install.sh: install dependencies
* start_tangodb.sh: start tango database (docker)
* start_dahu.sh: start Dahu server
* run.py: run a job

## Dependencies

Packages

* pytango
* dahu
* ewoks (for the plugin)

Infrastructure

* Tango database (docker)
